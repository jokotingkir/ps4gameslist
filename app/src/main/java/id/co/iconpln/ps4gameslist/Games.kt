package id.co.iconpln.ps4gameslist

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Games(
    var title: String = "",
    var genre: String = "",
    var desc: String = "",
    var developer: String = "",
    var publisher: String = "",
    var imgPreview: String = "",
    var youtubeTrailer: String = ""
) : Parcelable