package id.co.iconpln.ps4gameslist

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ListViewGameAdapter(private val context: Context, private val listGames: ArrayList<Games>) :
    BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(i: Int, v: View?, vg: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context).inflate(R.layout.item_list_games, vg, false)

        val viewHolder = ViewHolder(viewLayout)
        val game = getItem(i) as Games
        viewHolder.bind(context, game)

        return viewLayout
    }

    override fun getItem(i: Int): Any {
        return listGames[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getCount(): Int {
        return listGames.size
    }

    private inner class ViewHolder(view: View) {
        private val gameTitle: TextView = view.findViewById(R.id.tvGameTitle)
        private val gameGenre: TextView = view.findViewById(R.id.tvGameGenre)
        private val gameCover: ImageView = view.findViewById(R.id.ivImgPrev)
        private val gameDev: TextView = view.findViewById(R.id.tvGameDev)
        private val gamePub: TextView = view.findViewById(R.id.tvGamePub)

        fun bind(context: Context, game: Games) {
            gameTitle.text = game.title
            gameGenre.text = game.genre
            gameDev.text = game.developer
            gamePub.text = game.publisher

            Glide.with(context).load(game.imgPreview).apply(
                RequestOptions().fitCenter().placeholder(R.drawable.ic_launcher_foreground).error(R.drawable.ic_launcher_foreground)
            ).into(gameCover)
        }

    }

}