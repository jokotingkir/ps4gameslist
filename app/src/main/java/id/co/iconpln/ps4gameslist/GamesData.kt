package id.co.iconpln.ps4gameslist

object GamesData : Collection<Games> {

    val gamesList: ArrayList<Games>
        get() {
            val list = ArrayList<Games>()
            for (data in gamesData) {
                val games = Games()
                games.title = data[0]
                games.genre = data[1]
                games.desc = data[2]
                games.developer = data[3]
                games.publisher = data[4]
                games.imgPreview = data[5]
                games.youtubeTrailer = data[6]
                list.add(games)
            }

            return list
        }

    private var gamesData = arrayOf(
        arrayOf(
            "Assassin's Creed Chronicles",
            "Action-adventure",
            "Russia takes place in 1918 Russia during the aftermath of the October Revolution and features male protagonist Nikolai Orelov from comic book Assassin's Creed: Subject Four. Orelov wants to leave with his family, but is required to perform one last mission for the Assassin Order: infiltrate the house where the Tsar's family is being held by the Bolsheviks, and steal an artifact that has been fought over by Assassins and Templars for centuries. Along the way, he witnesses the massacre of the Tsar Nicholas II's children, but manages to save the grand duchess, Anastasia, who shares Shao Jun's memory. He must escape the Templars while protecting the artifact and Anastasia. Nikolai uses a custom Berdan rifle fitted with a bayonet which can be used for melee combat with sniping capabilities, and a grapnel gun that can also produce electricity in order to short out electricity boxes, spotlights, lamp posts or kill unsuspecting enemies standing on water puddles. Anastasia also becomes a playable character with unique set of skills, including the Helix Blade",
            "Climax Studios",
            "Ubisoft",
            "https://upload.wikimedia.org/wikipedia/en/8/8d/Assassin%27s_Creed_Chronicles_cover_art.jpg",
            "DnDICsPbO40"
        ),
        arrayOf(
            "Burnout Paradise Remastered",
            "Racing",
            "Paradise's gameplay is set in the fictional \"Paradise City\", an open world in which players can compete in several types of races. Players can also compete online, which includes additional game modes, such as \"Cops and Robbers\". Several free game updates introduce new features such as a time-of-day cycle and motorcycles. The game also features paid downloadable content in the form of new cars and the fictional \"Big Surf Island\".",
            "Stellar Entertainment",
            "Electronic Arts",
            "https://upload.wikimedia.org/wikipedia/en/a/ab/Burnout_Paradise_Boxart_2.jpg",
            "f4JqCmt9hzE"
        ),
        arrayOf(
            "Call of Duty: Infinite Warfare",
            "First-person shooter",
            "Special editions of Infinite Warfare were released with a remastered version of Call of Duty 4: Modern Warfare, entitled Modern Warfare Remastered. Infinite Warfare's announcement trailer was met with a mixed reception from game critics and journalists and a negative reception from the Call of Duty community. The announcement trailer was the second-most disliked video on YouTube at the time, in part due to a campaign by fans of rival game Battlefield 1 and disgruntled Call of Duty fans expressing frustration at the direction the series had taken, specifically that Infinite Warfare and its predecessors had vaguely similar futuristic settings. Nevertheless, the game received generally positive reviews upon release, critics praised the single-player campaign, which was seen as a considerable improvement over the game's predecessor Black Ops III, the greater player freedom approach in missions, the Zombies mode, and the visuals, while the multiplayer mode was criticized for its lack of innovation",
            "Infinity Ward",
            "Activision",
            "https://upload.wikimedia.org/wikipedia/en/f/fd/Call_of_Duty_-_Infinite_Warfare_%28promo_image%29.jpg",
            "MNGsb7IWEqk"
        ),
        arrayOf(
            "Darksiders II: Deathinitive Edition",
            "Action role-playing",
            "Darksiders II is parallel to the previous game. The Four Horsemen (War, Strife, Fury, and Death) are the last of the Nephilim, fusions of angels and demons who waged war on creation. To preserve the balance of the Universe, the Four, tired of conquest, received incredible powers from the Charred Council in exchange for slaughtering the rest of the Nephilim. The Horseman Death trapped the souls of his fallen brethren in an amulet, earning the title of Kinslayer, among others (though he kept its preservation a secret, since the Council ordered the Nephilim's souls destroyed).",
            "Gunfire Games",
            "Nordic Games",
            "https://upload.wikimedia.org/wikipedia/en/5/5c/DarksidersII.jpg",
            "WdXk_Lsyd7Q"
        ),
        arrayOf(
            "Destiny 2",
            "First-person shooter",
            "Players assume the role of a Guardian, protectors of Earth's last safe city as they wield a power called Light to protect the Last City from different alien races. One of these races, the Cabal, led by the warlord Dominus Ghaul, infiltrate the Last City and strips all Guardians of their Light. The player sets out on a journey to regain their Light and find a way to defeat Ghaul and his Red Legion army and take back the Last City. Like the original Destiny, the game features expansion packs which further the story and adds new content and missions. Year One of Destiny 2 featured two small expansions, Curse of Osiris in December 2017 and Warmind in May 2018. A third, large expansion, Forsaken, was released in September 2018, beginning Year Two with an overhaul on gameplay. The base game and the first three expansions were packaged into Destiny 2.",
            "Bungie",
            "Activision",
            "https://upload.wikimedia.org/wikipedia/en/0/05/Destiny_2_%28artwork%29.jpg",
            "8vt_hudGk0s"
        ),
        arrayOf(
            "EA Sports UFC 3",
            "Fighting",
            "EA Sports UFC 3 is a mixed martial arts fighting game, similar to previous installments, the game is based on Ultimate Fighting Championship (UFC), while also retaining realism with respect to physics, sounds and movements. The game has also been heavily endorsed by Conor McGregor, the cover athlete as well as one of the UFC's top stars. New to the series is the \"G.O.A.T.\" career mode, where the choices made throughout the career impact the player's path to greatness. Outside of fights, the player can make promotional choices to build hype, gain fans, earn more cash through big contracts and capture the world's attention. A new in-game social media system now allows the player to create heated rivalries with other UFC athletes, providing the freedom to take on any type of persona.",
            "EA Canada",
            "Electronic Arts",
            "https://upload.wikimedia.org/wikipedia/en/f/fc/EA_Sports_UFC_3_Official_Boxart.jpg",
            "Pgh7dGxVKWw"
        ),
        arrayOf(
            "Fallout 76",
            "Action role-playing",
            "Fallout 76 is Bethesda Game Studios' first online multiplayer game. Players may play individually or with a party of up to three others. The servers for the game are public dedicated servers, with the player automatically allocated to one of them. While the game was expected to launch with public servers only, game director Todd Howard revealed plans for private servers to be introduced some time after the game's launch. These private servers allow players to invite friends and to prevent undesirable aspects of player versus player gameplay such as griefing from affecting an individual player's experience of the game. Howard described the delay as being necessary to allow Bethesda time to assure the stability of public servers. Elements of previous Fallout games are present and are modified to work with the real-time game. The V.A.T.S. system—a mechanic first introduced in Fallout 3 that allows players to pause the game to target specific locations on an enemy's body to attack—is used in Fallout 76 as a real-time system, though it still allows players to specify targets on an enemy's body.",
            "Bethesda Game Studios",
            "Bethesda Softworks",
            "https://upload.wikimedia.org/wikipedia/en/0/06/Fallout_76_cover.jpg",
            "F0LWfDdYeCY"
        ),
        arrayOf(
            "FIFA 20",
            "Sports",
            "Gameplay changes to FIFA 20 focus primarily on a new feature titled VOLTA Football. The mode, which translates to 'return' in Portuguese, focuses on street football rather than the traditional matches associated with the FIFA series. It includes several options to play in three versus three, four versus four and five versus five matches, as well as with professional futsal rules.[10] The mode will incorporate the same engine, but places emphasis on skill and independent play rather than tactical or team play. Additionally, players have the option to customise their player by gender, clothing, shoes, hats and tattoos. Following the completion of the three-part series \"The Journey\" in FIFA 19, players can now have a similar storyline mode in VOLTA Football, which would be played with the player's own character.",
            "EA Vancouver",
            "Electronic Arts",
            "https://upload.wikimedia.org/wikipedia/en/2/21/FIFA_20_Standard_Edition_Cover.jpg",
            "rrSpkJhZSuU"
        ),
        arrayOf(
            "Killzone Shadow Fall",
            "First-person shooter",
            "Like its predecessors, Killzone Shadow Fall is a first-person shooter in a science fiction setting. Staple weapons such as the M82 Assault Rifle, stA-52 Assault Rifle, and stA-18 pistol return from the earlier Killzone games, albeit in new forms and variations. New weapons include the LSR44, a recoil-free, hybrid assault/charge sniper rifle that functions much like a miniaturized rail-gun, and the OWL, an advanced hovering attack drone (used by the Shadow Marshals) that can attack/stun when it is adjacent to an adversary, as well as deploy an instant zip line, protect the player from enemy fire with an energy shield, and hack/scan terminals and enemy alarms to prevent reinforcements from arriving.",
            "Guerrilla Games",
            "Sony Computer Entertainment",
            "https://upload.wikimedia.org/wikipedia/en/1/1e/Killzone_Shadow_Fall_Box.jpg",
            "JzyibMjn7YE"
        ),
        arrayOf(
            "Lords of the Fallen",
            "Action role-playing",
            "Lords of the Fallen is a third-person action role-playing game containing a slow tactical approach to close-quarters combat gameplay, with difficult enemies and locations to overcome, while learning from their encounters. The player takes the role of Harkyn, who, from the beginning, can be tailored towards the player's preferred combat styles from a range of different classes, each with their own specializations in certain weapons, armors, spells, and abilities. A class is determined based on two major choices of three kinds of magic; brawling, deception, and solace, followed by the second choice of three different armor sets; warrior, rogue, and cleric. Different combinations of both choices allow the player to choose how to play Harkyn from the start. With sets, the warrior uses heavy yet strong armor and weapons, the rogue is much lighter and quicker, and the cleric utilizes staffs and armour that supports spell use. Magic adds to the variability of each class. For example, a warrior set combined with brawling magic creates a pure warrior with high strength and vitality, while a warrior set can also be combined with solace magic to create a paladin class that is not as strong but can specialize in spell usage early in the game. As the player progresses through the game and defeats enemies, experience can be gained and spent to upgrade Harkyn's skills and unlock new spells to use in combat.",
            "Deck13 Interactive",
            "City Interactive",
            "https://upload.wikimedia.org/wikipedia/en/6/64/Lords_of_The_Fallen.png",
            "aBHBXOBKyAM"
        ),
        arrayOf(
            "God of War",
            "Action-adventure",
            "Kratos, Atreus, and Mímir's head journey to collect needed components to open Jötunheim's portal when they are attacked by Modi and Magni. After Kratos kills Magni, Modi flees, but later returns and ambushes them. Atreus collapses ill, which Mímir and Freya explain is a mental contradiction of a god believing himself to be mortal. She tells Kratos that he must retrieve the heart of the Keeper of the Bridge of the Damned in Helheim, but his Leviathan Axe is useless there. Kratos returns home to unearth his old weapons, the Blades of Chaos, and is haunted by Athena's spirit, who goads him about his past. After retrieving the heart, he has a haunting vision of Zeus. Atreus is cured, and Kratos tells him he is a god. Atreus becomes increasingly arrogant on their journey and murders a weakened Modi, despite Kratos ordering him not to. At Midgard's peak, they are ambushed by Baldur, resulting in Jötunheim's portal being destroyed and the group falling into Helheim. In the game's secret ending, Kratos and Atreus return home and slumber. Atreus has a vision that at the end of Fimbulwinter, Thor will arrive at their home to confront them.",
            "SIE Santa Monica Studio",
            "Sony Interactive Entertainment",
            "https://upload.wikimedia.org/wikipedia/en/a/a7/God_of_War_4_cover.jpg",
            "K0u_kAWLJOA"
        )
    )

    override val size: Int
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override fun contains(element: Games): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun containsAll(elements: Collection<Games>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun isEmpty(): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun iterator(): Iterator<Games> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}