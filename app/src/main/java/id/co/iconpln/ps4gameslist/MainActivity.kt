@file:Suppress("UNUSED_ANONYMOUS_PARAMETER")

package id.co.iconpln.ps4gameslist

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.ArrayList
import androidx.appcompat.widget.Toolbar
import kotlin.system.exitProcess

class MainActivity : AppCompatActivity() {

    private val listGames: ListView
        get() = lvGamesList

    private var list: ArrayList<Games> = arrayListOf()

    private var isAsc: Boolean = true
    private var doubleBack: Boolean = false

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setToolbar()

        loadList()
        val intent = Intent(this, DetailActivity::class.java)
        setItemClickListener(listGames, intent)
    }

    private fun setItemClickListener(listView: ListView, intent: Intent) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, index, id ->
                showItemDetail(intent, list[index])
            }
    }

    private fun setToolbar() {
        val mainToolbar: Toolbar = findViewById(R.id.mainToolbar)
        setSupportActionBar(mainToolbar)
        mainToolbar.title = actionBar?.title
    }

    private fun loadListBaseAdapter(context: Context) {
        list.clear()

        if (isAsc) {
            list.addAll(GamesData.gamesList.sortedWith(compareBy { it.title }))
        } else {
            list.addAll(GamesData.gamesList.sortedByDescending { it.title })
        }

        val baseAdapter = ListViewGameAdapter(context, list)
        listGames.adapter = baseAdapter
    }

    private fun loadListArrayAdapter() {
        val adapter: ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, getGamesData())

        listGames.adapter = adapter
    }

    private fun getGamesData(): Array<String> {

        return arrayOf(
            "Assassin's Creed Chronicles",
            "Burnout Paradise Remastered",
            "Call of Duty: Infinite Warfare",
            "Darksiders II: Deathinitive Edition",
            "Destiny 2",
            "EA Sports UFC 3",
            "Fallout 76",
            "FIFA 20",
            "Killzone Shadow Fall",
            "Lords of the Fallen",
            "God of War"
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_layout, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.sortGameList) {
            isAsc = !isAsc
        }

        loadList()

        val sorted = if (isAsc) "Sorted by Ascending" else "Sorted by Descending"
        Toast.makeText(this, sorted, Toast.LENGTH_LONG).show()

        return true
    }

    private fun loadList() {
        loadListArrayAdapter()
        loadListBaseAdapter(this)
    }

    private fun showItemDetail(intent: Intent, games: Games) {
        intent.putExtra(DetailActivity.GAME_DETAIL, games)
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (doubleBack) {
            finish()
            exitProcess(0)
        }

        this.doubleBack = true

        Toast.makeText(this, "Please press again to exit", Toast.LENGTH_LONG).show()
    }
}
