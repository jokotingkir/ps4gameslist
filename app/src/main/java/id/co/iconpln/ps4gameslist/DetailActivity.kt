package id.co.iconpln.ps4gameslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import kotlinx.android.synthetic.main.activity_detail.*
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailActivity : AppCompatActivity() {

    companion object {
        const val GAME_DETAIL = "game_detail"
    }

    private lateinit var games: Games

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setDetailToolbar()
        initIntentExtra()
        setDetailContent()
        setFullScreenYoutubePlayer()
    }

    private fun initIntentExtra() {
        games = intent.getParcelableExtra(GAME_DETAIL)
    }

    private fun setGlideImage() {
        Glide.with(this).load(games.imgPreview).apply(
            RequestOptions().fitCenter().placeholder(R.drawable.ic_launcher_foreground).error(
                R.drawable.ic_launcher_foreground
            )
        ).into(ivImgPreview)
    }

    private fun setDetailContent() {
        supportActionBar?.title = games.title
        tvDetailGameGenre.text = games.genre
        tvDetailGameDesc.text = games.desc
        tvDetailGameDev.text = games.developer
        tvDetailGamePub.text = games.publisher
        setGlideImage()
        setYoutubePlayer(games.youtubeTrailer)
    }

    private fun setDetailToolbar() {
        val detailToolbar: Toolbar = findViewById(R.id.detailToolbar)
        setSupportActionBar(detailToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setYoutubePlayer(vId: String) {
        tpYoutubePlayer.getPlayerUiController().showFullscreenButton(true)
        tpYoutubePlayer.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                youTubePlayer.cueVideo(vId, 0f)
            }
        })
    }

    private fun setFullScreenYoutubePlayer() {
        tpYoutubePlayer.getPlayerUiController()
            .setFullScreenButtonClickListener(View.OnClickListener {
                if (tpYoutubePlayer.isFullScreen()) {
                    tpYoutubePlayer.exitFullScreen()
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE

                    if (supportActionBar != null) supportActionBar!!.show()

                } else {
                    tpYoutubePlayer.enterFullScreen()
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

                    if (supportActionBar != null) supportActionBar!!.hide()
                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.share_button, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.shareGameDetail) {
            val sharedText =
                "https://www.youtube.com/watch?v=${games.youtubeTrailer} \n Game Title : ${games.title} \n Description${games.desc}"
            val shareIntent = Intent(Intent.ACTION_SEND)

            shareIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
            shareIntent.type = "text/plain"

            val intentShare = Intent.createChooser(shareIntent, "Share with")

            if (intentShare.resolveActivity(packageManager) != null)
                startActivity(intentShare)
            else
                Toast.makeText(
                    this,
                    "This button are used for share game info",
                    Toast.LENGTH_LONG
                ).show()

            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
