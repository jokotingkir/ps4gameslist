package id.co.iconpln.ps4gameslist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    companion object {
        private const val USERNAME = "user@mail.com"
        private const val PASSWORD = "password"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        btnLogin.setOnClickListener {
            isAuthorized()
        }
    }

    private fun isAuthorized() {

        if ((!isEmptyUsername() && validUsername()) && (!isEmptyPassword() && validPassword())) {
            if (tilUsername.editText?.text.toString() == USERNAME && tilPassword.editText?.text.toString() == PASSWORD) {
                val gamesIntent = Intent(this, MainActivity::class.java)
                startActivity(gamesIntent)
                finish()
            } else {
                tvLoginMessage.setText(R.string.invalid_login)
            }
        }
    }

    private fun isEmptyUsername(): Boolean {
        val isEmptyUsername = tilUsername.editText?.text.toString().isEmpty()
        return if (isEmptyUsername) {
            tilUsername.error = "Username required"
            true
        } else {
            tilUsername.error = null
            false
        }
    }

    private fun isEmptyPassword(): Boolean {
        val isEmptyPassword = tilPassword.editText?.text.toString().isEmpty()

        return if (isEmptyPassword) {
            tilPassword.error = "Password required"
            true
        } else {
            tilPassword.error = null
            false
        }
    }

    private fun validUsername(): Boolean {
        return if (!tilUsername.editText?.text.toString().isValidEmail()) {
            tilUsername.error = "Username is not valid"
            false
        } else {
            tilUsername.error = null
            true
        }
    }

    private fun validPassword(): Boolean {
        return if (!tilPassword.editText?.text.toString().isValidPassword()) {
            tilPassword.error = "Minimal character is 7"
            false
        } else {
            tilPassword.error = null
            true
        }
    }

    private fun String.isValidEmail(): Boolean = Patterns.EMAIL_ADDRESS.matcher(this).matches()

    private fun String.isValidPassword(): Boolean = this.length >= 7
}
